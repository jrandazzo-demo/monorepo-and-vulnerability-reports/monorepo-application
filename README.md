# Vulnerability reporting with a monorepo

When developing within a monorepo you may have a team working out of a specific directory or branch and would like to have a generated vulnerability report aligned to them. As of 15.0, the vulnerability report is reflective of the default branch.  This example shows how to generate a separate report for the branch or directory (WIP) using multi project pipelines and `needs:projects`.

![Diagram](/res/diagram.png "Diagram")


## Creating a report for a unique branch (non default)


The scanning job will run within the monorepo application. The artifact(gl-secret-detection-report.json) will be passed into the reporting project based on this branch run. See this [project ](https://gitlab.com/jrandazzo-demo/monorepo-and-vulnerability-reports/team-x-vulnerability-report)on how the artifact is retrieved and displayed as a report.

[Epic tracking native capability](https://gitlab.com/groups/gitlab-org/-/epics/3430)

### CI file for source repo 
```yaml
# example: https://gitlab.com/jrandazzo-demo/monorepo-and-vulnerability-reports/monorepo-application/-/blob/TeamX_Branch/.gitlab-ci.yml
include:
  - template: Security/Secret-Detection.gitlab-ci.yml

stages:
  - test
  - report

# `paths` must be specified instead of `report` for downstream project to download
secret_detection:
  artifacts:
    paths:
      - gl-secret-detection-report.json

#trigger downstream reporting project
send-report:
  stage: report
  trigger: jrandazzo-demo/monorepo-and-vulnerability-reports/team-x-vulnerability-report
```

### CI file for reporting repository

```yaml
# Capture secret detection report from monorepo branch for this project to display
secret-detection-report:
  script:
    - echo "Dumby job to download json report from secrets detection of monorepo project"
    - cat gl-secret-detection-report.json
  needs:
    - project: jrandazzo-demo/monorepo-and-vulnerability-reports/monorepo-application
      job: secret_detection
      ref: TeamX_Branch
      artifacts: true
  artifacts:
    reports:
      secret_detection: gl-secret-detection-report.json

```

CI References
* [Multi Project Pipelines](https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html)
* [needs:project](https://docs.gitlab.com/ee/ci/yaml/index.html#needsproject)
* Hide [visibility of controls](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) in reporting project to simplify


## Creating a report for a directory (WIP)


## Important Caveats
* The vulnerability report will be separate from the code base. Given this, the file pointer in the vulnerability object will not work.
* The activity on the vulnerability object may misidentify as "remediated" since the code does not exist in the vulnerability report project.
